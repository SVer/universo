extends Node



# URL к API
const URL = "https://t34.tehnokom.su/api/v1.1/"


# Запрос к API, 
func objecto_query(statusoId, kategorioId, tipoId, kuboId = 1):
	return JSON.print({ "query": "query ($kuboId:Float, $statusoId:Float, "+
		" $realecoId:Float, $kategorioId:Float, $tipoId:Float) " +
		"{ universoObjekto (realeco_Id:$realecoId, kubo_Id: $kuboId, "+
		" universoObjektojUniversoobjektoligiloLigiloObjekto_Isnull:true, "+
		" koordinatoX_Isnull:false " +
		" ) { edges { node { uuid posedantoId "+
		" projekto (statuso_Id: $statusoId, tipo_Id: $tipoId){ "+
		"  edges { node { uuid "+
		"  tasko (kategorio_Id:$kategorioId){ edges {node { "+
		"   uuid finKoordinatoX finKoordinatoY finKoordinatoZ statuso {objId} } } } } } } "+
		" nomo { enhavo } priskribo { enhavo } "+
		" resurso { objId nomo { enhavo } priskribo { enhavo } "+
		"  tipo { objId nomo { enhavo } } "+
		"  ligilo { edges { node { ligilo { "+
		"   objId nomo{enhavo} priskribo{enhavo} "+
		"   tipo{ objId nomo{enhavo} } } } } } "+
		" } "+
		" koordinatoX koordinatoY koordinatoZ "+
		' posedantoObjekto '+
		'  { uuid } '+
		" nomo{enhavo}" +
		" ligilo{edges{node{ligilo{ nomo{enhavo} ligilo{edges{node{" +
		"  konektiloPosedanto konektiloLigilo" +
		"  ligilo{ uuid }}}}}" +
		"  tipo{objId}}}}" +
		" posedanto{edges{node{" +
		"  posedantoUzanto{ siriusoUzanto{ objId}}}}}" +
		" rotaciaX rotaciaY rotaciaZ } } } }",
		'variables': {"kuboId":kuboId, "statusoId":statusoId, 
		"kategorioId":kategorioId, "tipoId":tipoId,
		"realecoId":Global.realeco} })
		
		
# запрос на список управляемых объектов
func get_direktebla_query(statusoId, kategorioId, tipoId):
	var query = JSON.print({ "query": "query ($UzantoId:Int, $statusoId:Float, "+
		" $kategorioId:Float, $tipoId:Float)"+
		"{ universoObjekto (" +
		" universoobjektouzanto_Isnull:false,"+
		" universoobjektouzanto_Autoro_SiriusoUzanto_Id:$UzantoId," +
		") { edges { node { uuid " +
		" projekto (statuso_Id: $statusoId, tipo_Id: $tipoId){ "+
		"  edges { node { uuid "+
		"  tasko (kategorio_Id:$kategorioId){ edges {node { "+
		"   uuid finKoordinatoX finKoordinatoY finKoordinatoZ statuso {objId} } } } } } } "+
		" nomo { enhavo } priskribo { enhavo } "+
		" resurso { objId nomo { enhavo } priskribo { enhavo } "+
		"  tipo { objId nomo { enhavo } } "+
		"  ligilo { edges { node { ligilo { "+
		"   objId nomo{enhavo} priskribo{enhavo} "+
		"   tipo{ objId nomo{enhavo} } } } } } "+
		" } "+
		" koordinatoX koordinatoY koordinatoZ "+
		' posedantoObjekto '+
		'  { uuid } '+
		" nomo{enhavo}" +
		" ligiloLigilo{edges{node{uuid "+
		"  posedanto{ kubo {objId} koordinatoX koordinatoY koordinatoZ }}}}" +
		" ligilo{edges{node{ ligilo{ nomo{enhavo} ligilo{edges{node{" +
		"  konektiloPosedanto konektiloLigilo" +
		"  ligilo{ uuid }}}}}" +
		"  tipo{objId}}}}" +
		" realeco{objId}" +
		" posedanto{edges{node{" +
		"  posedantoUzanto{ siriusoUzanto{ objId}}}}}" +
		" rotaciaX rotaciaY rotaciaZ } } } }",
		'variables': {"statusoId":statusoId, 
		"kategorioId":kategorioId, "tipoId":tipoId,
		"UzantoId":Global.id} })

	# print("=== get_direktebla_query = ",query)
	return query
	
