extends Spatial

var choose = true

signal load_objektoj

func _ready():
	# считываем размер экрана и задаём затемнение на весь экран
	$ui/loading.margin_right = get_node("/root").get_viewport().size.x
	$ui/loading.margin_bottom = get_node("/root").get_viewport().size.y
	$b_itinero.margin_left = get_node("/root").get_viewport().size.x - 100
	$b_itinero.margin_top = get_node("/root").get_viewport().size.y-50
	# создаём свой корабль
	var ship = create_ship(Global.direktebla_objekto[Global.realeco-2])
	#если корабль игрока, то брать данные из direktebla_objekto

	$main_camera.translation=Vector3(
		Global.direktebla_objekto[Global.realeco-2]['koordinatoX'],
		Global.direktebla_objekto[Global.realeco-2]['koordinatoY'], 
		Global.direktebla_objekto[Global.realeco-2]['koordinatoZ']+22
	)
	add_child(ship,true)

	for i in get_children():
		if has_signal(i,"new_way_point"):
			i.connect("new_way_point",self,"set_way_point")

func _input(event):
	if Input.is_action_just_pressed("ui_select"):
		choose=!choose
		if choose:
			$main_camera.set_privot($ship)
		else:
			$main_camera.set_privot(null)

func set_way_point(position,dock):
	get_node("ship").add_itinero()
	#останавливаем таймер
	$timer.stop()
	$ship.set_way_point(position,dock)
	$way_point.set_way_point(position)
	#передаём текущие координаты
	$ship.vojkomenco()#начинаем движение
	#запускаем таймер
	$timer.start()

func has_signal(node, sgnl):
	if node == null:
		return false
	node=node.get_signal_list()
	for i in node:
		if i.name == sgnl:
			return true
	return false




const QueryObject = preload("queries.gd")


# записав проект в базу, получили его uuid
func _on_http_projekto_request_completed(result, response_code, headers, body):
	var resp = body.get_string_from_utf8()
	var parsed_resp = parse_json(resp)
	var simpled_data = parsed_resp['data']['redaktuUniversoProjekto']['universoProjekto']
	var uuid = simpled_data['uuid']
	$"ship".projekto_uuid=uuid
	# теперь создаём задачу с координатами
	var q = QueryObject.new()
	Global.direktebla_objekto[Global.realeco-2]['koordinatoX'] = $ship.translation.x
	Global.direktebla_objekto[Global.realeco-2]['koordinatoY'] = $ship.translation.y
	Global.direktebla_objekto[Global.realeco-2]['koordinatoZ'] = $ship.translation.z
	Global.direktebla_objekto[Global.realeco-2]['rotationX'] = $ship.rotation.x
	Global.direktebla_objekto[Global.realeco-2]['rotationY'] = $ship.rotation.y
	Global.direktebla_objekto[Global.realeco-2]['rotationZ'] = $ship.rotation.z
	$"http_taskoj".request(q.URL, Global.backend_headers, true, 2,
		q.instalo_tasko_posedanto_koord(
			$"ship".uuid, $"ship".projekto_uuid, 
			$"ship".translation.x, #kom_koordX
			$"ship".translation.y, #kom_koordY
			$"ship".translation.z, #kom_koordZ
			$"b_itinero/itinero".itineroj
	))




func _on_http_tasko_request_completed(result, response_code, headers, body):
	var resp = body.get_string_from_utf8()
	var parsed_resp = parse_json(resp)
	var simpled_data = parsed_resp['data']['redaktuUniversoTaskoj']['universoTaskoj']
	# получаем список задач и помещаем в itinero
	$"b_itinero/itinero".itineroj[0]['uuid_tasko']=simpled_data['uuid']


func _on_http_posedanto_request_completed(result, response_code, headers, body):
	var resp = body.get_string_from_utf8()
	var parsed_resp = parse_json(resp)
	pass


func _on_http_finado_request_completed(result, response_code, headers, body):
	var resp = body.get_string_from_utf8()
	var parsed_resp = parse_json(resp)
	pass # Replace with function body.


func _on_Button_pressed():
	if $"b_itinero/itinero/canvas/MarginContainer".visible:
		$"b_itinero/itinero/canvas/MarginContainer".visible = false
	else:
		# устанавливаем координаты окна
		$"b_itinero/itinero/canvas/MarginContainer".margin_left = $b_itinero.margin_left - 200
		$"b_itinero/itinero/canvas/MarginContainer".margin_right = get_node("/root").get_viewport().size.x
		$"b_itinero/itinero/canvas/MarginContainer".margin_top = $b_itinero.margin_top - 200
		$"b_itinero/itinero/canvas/MarginContainer".margin_bottom = $b_itinero.margin_top
		$"b_itinero/itinero/canvas/MarginContainer".visible = true


func _on_http_taskoj_request_completed(result, response_code, headers, body):
	var resp = body.get_string_from_utf8()
	var parsed_resp = parse_json(resp)
	var simpled_data = parsed_resp['data']['redaktuKreiUniversoTaskojPosedanto']['universoTaskoj']
	# получаем список задач и помещаем в itinero
	var i = 0
	for tasko in simpled_data:
		if len($"b_itinero/itinero".itineroj)>i:
			$"b_itinero/itinero".itineroj[i]['uuid_tasko']=tasko['uuid']
		i += 1


const ships = preload("res://blokoj/kosmo/scenoj/ships.tscn")
const space_object = preload("res://blokoj/kosmo/scenoj/space_object.tscn")
const ship_gem = preload("res://blokoj/kosmo/scenoj/ship.tscn")

const base_ship = preload("res://blokoj/kosmosxipoj/scenoj/base_ship.tscn")
const base_cabine = preload("res://blokoj/kosmosxipoj/scenoj/moduloj/cabine.tscn")
const base_engines = preload("res://blokoj/kosmosxipoj/scenoj/moduloj/engines.tscn")
const base_cargo = preload("res://blokoj/kosmosxipoj/scenoj/moduloj/cargo.tscn")

const espero = preload("res://blokoj/kosmostacioj/espero/espero_ekster.tscn")

# функция создания корабля
func create_ship(objecto):
	var ship = null
	var cabine = null
	var cargo = null
	var engines = null
	if (objecto['resurso']['objId'] == 3):# это корабль "Vostok U2" "Базовый космический корабль"
		ship = base_ship.instance()
		engines = base_engines.instance()
		ship.get_node("CollisionShape").add_child(engines)
		for modulo in objecto['resurso']['ligilo']['edges']:
			if modulo['node']['ligilo']['objId'] == 4: #"Vostok Модуль Кабины" "Базовый модуль кабины кораблей Vostok"
				cabine = base_cabine.instance()
				ship.get_node("CollisionShape").add_child(cabine)
			if modulo['node']['ligilo']['objId'] == 5: #"Vostok Грузовой Модуль" "Базовый грузовой модуль кораблей Vostok"
				cargo = base_cargo.instance()
				ship.get_node("CollisionShape").add_child(cargo)
	if not ship: # проверка, если такого корабля нет в программе
		return ship
	ship.translation=Vector3(objecto['koordinatoX'],
		objecto['koordinatoY'], objecto['koordinatoZ'])
	if objecto['rotaciaX']:
		ship.rotation=Vector3(objecto['rotaciaX'],
			objecto['rotaciaY'], objecto['rotaciaZ'])
	ship.visible=true
	ship.uuid=objecto['uuid']
	ship.add_to_group('create')
	$main_camera.set_privot(ship)
	return ship


func _on_space_load_objektoj():
	Title.get_node("CanvasLayer/UI/L_VBox/Objektoj/Window").distance_to($ship.translation)

	# и теперь по uuid нужно найти проект и задачу
	var projektoj = Global.direktebla_objekto[Global.realeco-2]['projekto']['edges']
	if len(projektoj)>1:
		pass #нужно обнулить все проекты!!!
	if len(projektoj)>0:
		if len(projektoj[0]['node']['tasko']['edges'])==0:
			pass# нужно закрыть проект!!!
		else:
			$ship.projekto_uuid=projektoj[0]['node']['uuid']
			#заполняем маршрут
			$"b_itinero/itinero".itineroj.clear()
			for tasko in projektoj[0]['node']['tasko']['edges']:
				if tasko['node']['statuso']['objId']==2:#задачу, которая "В работе" ставим первой
					$"b_itinero/itinero".add_itinero(
						tasko['node']['uuid'],
						'',
						'координаты в космосе',
						tasko['node']['finKoordinatoX'],
						tasko['node']['finKoordinatoY'],
						tasko['node']['finKoordinatoZ'],
						$ship.translation.distance_to(Vector3(
							tasko['node']['finKoordinatoX'],
							tasko['node']['finKoordinatoY'],
							tasko['node']['finKoordinatoZ']
					)))
					break;
			for tasko in projektoj[0]['node']['tasko']['edges']:
				if tasko['node']['statuso']['objId']==1:# добавляем остальные задачи
					$"b_itinero/itinero".add_itinero(
						tasko['node']['uuid'],
						'',
						'координаты в космосе',
						tasko['node']['finKoordinatoX'],
						tasko['node']['finKoordinatoY'],
						tasko['node']['finKoordinatoZ'],
						$ship.translation.distance_to(Vector3(
							tasko['node']['finKoordinatoX'],
							tasko['node']['finKoordinatoY'],
							tasko['node']['finKoordinatoZ']
					)))
			if len($"b_itinero/itinero".itineroj)==0:
				$ship.projekto_uuid='' #задач на полёт нет, проект надо бы закрыть
			else:
				#отправляем корабль по координатам
				var position = Vector3($"b_itinero/itinero".itineroj[0]['koordinatoX'],
					$"b_itinero/itinero".itineroj[0]['koordinatoY'],
					$"b_itinero/itinero".itineroj[0]['koordinatoZ'])
				$"b_itinero/itinero".FillItemList()
				$ship.set_way_point(position,null)
				$"way_point".set_way_point(position)
				$timer.start()
				$"b_itinero/itinero/canvas/MarginContainer/VBoxContainer/HBoxContainer/kom_itinero".disabled=true
				$"b_itinero/itinero/canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_next".disabled=false
				$"b_itinero/itinero/canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_fin".disabled=false
				$"b_itinero/itinero/canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_clear".disabled=false
	else:
		$ship.projekto_uuid=''
	
	# создаём остальные объекты в космосе
	for item in Global.objektoj:
		if item['resurso']['objId'] == 1:#объект станция Espero
			var state = espero.instance()
			state.translation=Vector3(item['koordinatoX'],
				item['koordinatoY'], item['koordinatoZ'])
			add_child(state)
			state.add_to_group('state')
			state.add_to_group('create')
		if (item['resurso']['tipo']['objId'] == 2)and(item['koordinatoX']):# тип - корабль
			if (item['koordinatoX']):
				var s = ships.instance()
				s.translation=Vector3(item['koordinatoX'],
					item['koordinatoY'], item['koordinatoZ'])
				s.rotation=Vector3(item['rotaciaX'],
					item['rotaciaY'], item['rotaciaZ'])
				add_child(s)
				s.add_to_group('create')
				s.add_to_group('enemies')

	pass # Replace with function body.


func _on_Timer_timeout():
	var q = QueryObject.new()
	# Делаем запрос к бэкэнду
	Global.direktebla_objekto[Global.realeco-2]['koordinatoX'] = $ship.translation.x
	Global.direktebla_objekto[Global.realeco-2]['koordinatoY'] = $ship.translation.y
	Global.direktebla_objekto[Global.realeco-2]['koordinatoZ'] = $ship.translation.z
	Global.direktebla_objekto[Global.realeco-2]['rotationX'] = $ship.rotation.x
	Global.direktebla_objekto[Global.realeco-2]['rotationY'] = $ship.rotation.y
	Global.direktebla_objekto[Global.realeco-2]['rotationZ'] = $ship.rotation.z
	$http_mutate.request(q.URL, Global.backend_headers, true, 2, 
		q.objecto_mutation($ship.uuid, $ship.translation.x, 
			$ship.translation.y, $ship.translation.z,
			$ship.rotation.x, 
			$ship.rotation.y, $ship.rotation.z)
	)



func _on_space_tree_exiting():
	#разрушаем все созданные объекты в этом мире
	for ch in get_children():
		if ch.is_in_group('create'):
			ch.free()
